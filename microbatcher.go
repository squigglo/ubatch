// Package microbatcher provides simple micro batching functionality.
// Microbatching is the process of grouping individual jobs into batches, to limit the number of requests made to a
// downstream processor.
package microbatcher

import (
	"fmt"
	"sync"
	"time"
)

// In reality we might want Jobs to be more useful than strings, but hopefully it is for this exercise
type Job string
type JobResult string

type jobRequest struct {
	Job
	result chan JobResult
}

type BatchProcessor func([]Job) []JobResult

type MicroBatcher struct {
	sync.Mutex              // used to prevent new jobs from being accepted while the micro batcher is shutting down
	maxJobs          int
	maxWait          time.Duration
	processor        BatchProcessor
	request          chan jobRequest // used to send/recv jobs/results to procPump
	shutdownChan     chan bool       // to indicate a shutdown has been requested
	procPumpFinished chan bool       // to indicate the procPump has finished
	isShutdown       bool            // to indicate that the micro batcher has completed shutting down
}

// Constructs a micro batcher and starts the batching goroutine
func NewMicroBatcher(maxJobs int, maxWait time.Duration, proc BatchProcessor) *MicroBatcher {
	mb := MicroBatcher{sync.Mutex{}, maxJobs, maxWait, proc, make(chan jobRequest), make(chan bool), make(chan bool), false}
	go mb.processPump()

	return &mb
}

// Handles job batching and processing.
// Will run forever until shutdownChan returns a value.
func (mb *MicroBatcher) processPump() {
	jobs := make([]Job, 0, mb.maxJobs)
	resultChans := make([]chan JobResult, 0, mb.maxJobs)

	// processes the current batch, and prepares the next one
	doProc := func() {
		results := mb.processor(jobs)
		for i, r := range results {
			resultChans[i] <- r
		}
		jobs = jobs[:0]
		resultChans = resultChans[:0]
	}

	for !mb.isShutdown {
		select {
		case jr := <-mb.request:
			jobs = append(jobs, jr.Job)
			resultChans = append(resultChans, jr.result)
			if len(jobs) == mb.maxJobs {
				doProc()
			}
		case <-mb.shutdownChan:
			doProc()
			mb.isShutdown = true
		case <-time.After(mb.maxWait):
			doProc()
		}
	}
	mb.procPumpFinished <- true
}

// Queues a job to the next batch to be executed, waits for it to run, and returns the result.
// Returns an error if the MicroBatcher has shutdown.
func (mb *MicroBatcher) RunJob(j Job) (JobResult, error) {
	mb.Lock()	// lock to prevent a shutdown race resulting in this job being queued without ever being processed
	if mb.isShutdown {
		return "", fmt.Errorf("shutdown, cannot accept new jobs")
	}
	result := make(chan JobResult)
	mb.request <- jobRequest{j, result}
	mb.Unlock()

	return <-result, nil
}

// Waits for any pending jobs to complete, and then returns.
// No future jobs may be run after this has completed.
// Does not guarantee that any active calls to RunJob have returned, only that they have been processed and will be
// returned without needing to use the processor again.
func (mb *MicroBatcher) Shutdown() {
	mb.Lock()
	defer mb.Unlock()
	mb.shutdownChan <- true
	<-mb.procPumpFinished
	mb.isShutdown = true
}
