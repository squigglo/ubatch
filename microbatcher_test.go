package microbatcher

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

// Simple job processor that returns results that match the job but with "Processed: " prepended to the front.
func proc(jobs []Job) []JobResult {
	results := make([]JobResult, len(jobs))
	for i, val := range jobs {
		results[i] = JobResult(fmt.Sprintf("Processed: %v", val))
	}
	return results
}

// runs a single job, and checks it is processed correctly
func runAndCheckJob(t *testing.T, mb *MicroBatcher, job Job) {
	result, err := mb.RunJob(job)
	if err != nil {
		t.Errorf("RunJob returned error: %v", err)
	}

	expected := JobResult(fmt.Sprintf("Processed: %v", job))
	if result != expected {
		t.Errorf("Job returned an inccorect result, expected: %v, got: %v", expected, result)
	}
}

// returns (sub)test that runs a configurable number of jobs and checks they all complete successfully
func runJobSeries (batchSize int, maxWait time.Duration, jobsToRun int,) func (t *testing.T){
	return func(t *testing.T) {
		mb := NewMicroBatcher(batchSize, maxWait, proc)

		queue := func(i int, wg *sync.WaitGroup) {
			runAndCheckJob(t, mb, Job(fmt.Sprintf("job %v", i)))
			wg.Done()
		}
		wg := sync.WaitGroup{}
		for i := 0; i < jobsToRun; i++ {
			wg.Add(1)
			go queue(i, &wg)
		}
		wg.Wait()
	}
}

// tests three batches of jobs, checking that the correct result is returned.
func TestManyJobs(t *testing.T) {
	t.Run("1 job, 1 per batch", runJobSeries(1, time.Millisecond * 20, 1))
	t.Run("100 jobs, 2 per batch", runJobSeries(2, time.Millisecond * 20, 100))
	t.Run("100 jobs, 10 per batch", runJobSeries(10, time.Millisecond * 20, 100))
	t.Run("10000 jobs, 100 per batch", runJobSeries(10000, time.Millisecond * 100, 100))
	t.Run("100 jobs, 200 per batch", runJobSeries(200, time.Millisecond * 100, 100))
}

// Checks that a timeout can trigger a job to run
func TestTimeout(t *testing.T) {
	mb := NewMicroBatcher(10, time.Millisecond * 500, proc)
	start := time.Now()
	runAndCheckJob(t, mb, "test")
	elapsed := time.Since(start)
	if elapsed < time.Millisecond * 250 {
		t.Errorf("Job finished early in: %v ", elapsed)
	}
}

func TestShutdownBlocksJobs(t *testing.T) {
	mb := NewMicroBatcher(60, 60*time.Second, proc)
	mb.Shutdown()
	_, err := mb.RunJob("test")
	if err == nil {
		t.Error("RunJob didn't return an error when the batcher was shutdown")
	}
}

// checks that a shutdown immediately triggers the jobs to be processed
func TestShutdownTriggersBatch(t *testing.T) {
	mb := NewMicroBatcher(60, 60*time.Second, proc)

	queueJob := func() {
		// disregard the returned values because some may fail due to the racing shutdown
		_, _ = mb.RunJob("test")
	}

	for i := 0; i < 15; i++ {
		go queueJob()
	}

	// Now lets ensure that shutdown happens "quickly" compared to the wait time
	// The allowed duration is still very generous to defend against *very* slow test runners
	startTime := time.Now()
	mb.Shutdown()
	elapsedTime := time.Since(startTime)
	if time.Since(startTime) > time.Second*10 {
		t.Errorf("Shutdown took too long to return (%v), implying it didn't work", elapsedTime)
	}
}
