# Microbatch
This is an implementation of a micro batching library.

It was written in an evening as part of a tech challenge for a company I was interviewing with.

It was quite fun to write in Go because the problem fits so well into Channels and the select statement.

# Future plans
- There are of course parts of the code that could be improved, in particular:
  - eliminating the mutex
  - changing the `Job` type
- I might look into setting up a simple CI/CD pipeline.
- Maybe I'll get around to building some toy service out of it that lives on AWS.

